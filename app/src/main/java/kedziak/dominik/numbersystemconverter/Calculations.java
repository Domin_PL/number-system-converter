package kedziak.dominik.numbersystemconverter;

/*
 * This class does all the calculations which are needed in this app
 */
public class Calculations {

    public int convert(int oldBase, int newBase, int number){

        return 0;
    }

    public int convertToDecimal(int number, int base){
        StringBuilder builder = new StringBuilder();
        String mNumber = builder.append(number).reverse().toString();
        int outcome = 0;

        if (base != 10){
            char[] chars = mNumber.toCharArray();
            int length = chars.length;
            for (int i = 0; i < length; i++) {
                int num = (int) chars[i] - '0';
                num = (int) (num * ((Math.pow(base, i))));
                outcome += num;
            }
        }
        return outcome;
    }

}
