package kedziak.dominik.numbersystemconverter;

import androidx.lifecycle.ViewModel;

/**
 * This class is a ViewModel which holds variables' values.
 * It also sends data to repository where calculations are being done
 */
public class NumberViewModel extends ViewModel {

    // It is a base of a number, means numeral notation that number is saved.
    // 10 - decimal, 2 - binary etc
    private int baseNumber;

    // Base to which number is to be convert
    private int baseToConvert;

    // number to be convert
    private int numberToConvert;

    // Number after conversions
    private int convertedNumber;




    public int getBaseNumber() {
        return baseNumber;
    }

    public void setBaseNumber(int baseNumber) {
        this.baseNumber = baseNumber;
    }

    public int getBaseToConvert() {
        return baseToConvert;
    }

    public void setBaseToConvert(int baseToConvert) {
        this.baseToConvert = baseToConvert;
    }

    public int getNumberToConvert() {
        return numberToConvert;
    }

    public void setNumberToConvert(int numberToConvert) {
        this.numberToConvert = numberToConvert;
    }

    public int getConvertedNumber() {
        return convertedNumber;
    }

    public void setConvertedNumber(int convertedNumber) {
        this.convertedNumber = convertedNumber;
    }
}
