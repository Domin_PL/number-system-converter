package kedziak.dominik.numbersystemconverter;

import android.content.Intent;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculationsTest {


    @Test
    public void convert() {
        int oldBase = 4; int newBase = 8; int number = 1233;

        //convert to decimal if it isn't
        int decimal = convertToDecimal(oldBase, number);

        // convert to given base
        int output = convertToAnyBase(newBase, decimal);

        assertEquals(157, output, -1);
    }


    // converts decimal number to number of any base.
    /**
     * @param newBase base to which number will be converted
     * @param number given in decimal number system
     * @return converted number
     */
    private int convertToAnyBase(int newBase, int number){
        StringBuilder builder = new StringBuilder();
        while (number != 0){
            builder.append((number % newBase));
            number /= newBase;
        }
        return Integer.parseInt(builder.reverse().toString());
    }


    @Test
    public void ConvertToDecimal() {
        int base = 4;
        int number = 1233;
        StringBuilder builder = new StringBuilder();
        String mNumber = builder.append(number).reverse().toString();
        int outcome = 0;

        if (base != 10){
            char[] chars = mNumber.toCharArray();
            int length = chars.length;
            for (int i = 0; i < length; i++) {
                int num = (int) chars[i] - '0';
                num = (int) (num * ((Math.pow(base, i))));
                outcome += num;
            }
        }
        assertEquals(111, outcome);
    }

    /**
     * This method converts given number to decimal number system
     * @param base actual number base
     * @param number given number to convert
     * @return converted number
     */
    private int convertToDecimal(int base, int number){
        StringBuilder builder = new StringBuilder();
        String mNumber = builder.append(number).reverse().toString();
        int outcome = 0;

        if (base != 10){
            char[] chars = mNumber.toCharArray();
            int length = chars.length;
            for (int i = 0; i < length; i++) {
                int num = (int) chars[i] - '0';
                num *= ((Math.pow(base, i)));
                outcome += num;
            }
        }
        return outcome;
    }


}

